package com.task.service.impl;


import com.task.entity.Book;
import com.task.repository.BookRepository;
import com.task.service.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
class BookServiceImplTest {

    private Book temp;
    @Autowired
    private BookRepository repository;

    private BookService service;

    @BeforeEach
    void createBook() {
        service = new BookServiceImpl(repository);
        temp = new Book();
        temp.setPrice(new BigDecimal(145));
        temp.setTitle("Test Book");
    }

    @Test
    void whenFindByIdThenReturnOne() {
        Book book = service.createBook(temp);
        temp.setId(book.getId());
        assertEquals(book, temp);
    }

    @Test
    void whenCreateThenGenerateId() {
        assertNull(temp.getId());
        assertNotNull(service.createBook(temp).getId());
    }

    @Test
    void whenDeleteByIdNotConsistRecordThenThrowException(){
        assertThrows(EmptyResultDataAccessException.class, ()-> service.deleteById(100L));
    }

    @Test
    void whenUpdateThenOverRecordById() {
        temp.setId(2L);
        assertNotEquals(service.getById(2L).get(), temp);
        service.update(temp);
        assertEquals(service.getById(2L).get(), temp);
    }
}
