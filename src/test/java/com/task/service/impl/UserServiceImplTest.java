package com.task.service.impl;

import com.task.entity.User;
import com.task.repository.UserRepository;
import com.task.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
public class UserServiceImplTest {

    private User temp;
    @Autowired
    private UserRepository repository;

    private UserService service;

    @BeforeEach
    void createBook() {
        service = new UserServiceImpl(repository);
        temp = new User();
        temp.setFirstName("Alex");
        temp.setLastName("Smith");

    }

    @Test
    void whenFindByIdThenReturnOne() {
        User user = service.createUser(temp);
        temp.setId(user.getId());
        assertEquals(user, temp);
    }

    @Test
    void whenCreateThenGenerateId() {
        assertNull(temp.getId());
        assertNotNull(service.createUser(temp).getId());
    }

    @Test
    void whenDeleteByIdNotConsistRecordThenThrowException() {
        assertThrows(EmptyResultDataAccessException.class, () -> service.deleteById(100L));
    }

    @Test
    void whenUpdateThenOverRecordById() {
        temp.setId(2L);
        assertNotEquals(service.getById(2L).get(), temp);
        service.update(temp);
        assertEquals(service.getById(2L).get(), temp);
    }
}
