package com.task.service.impl;

import com.task.entity.User;
import com.task.repository.UserRepository;
import com.task.service.UserService;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Data
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    @Override
    public Optional<User> getById(Long id) {
        return Optional.of(repository.getReferenceById(id));
    }

    @Override
    public User createUser(User user) {
        return repository.save(user);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public User update(User user) {
        return repository.save(user);
    }

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }
}
