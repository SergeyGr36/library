package com.task.service.impl;

import com.task.entity.Book;
import com.task.repository.BookRepository;
import com.task.service.BookService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Data
public class BookServiceImpl implements BookService {
    private final BookRepository repository;

    @Override
    public Optional<Book> getById(Long id) {
        return Optional.of(repository.getReferenceById(id));
    }

    @Override
    public Book createBook(Book book) {
        return repository.save(book);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Book update(Book book) {
        return repository.save(book);
    }

    @Override
    public List<Book> getAll() {
        return repository.findAll();
    }
}
