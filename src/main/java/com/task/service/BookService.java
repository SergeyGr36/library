package com.task.service;

import com.task.entity.Book;

import java.util.List;
import java.util.Optional;

public interface BookService {
    Optional<Book> getById(Long id);
    Book createBook(Book book);
    void deleteById(Long id);
    Book update(Book book);
    List<Book> getAll();
}
