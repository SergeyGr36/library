package com.task.service;

import com.task.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> getById(Long id);
    User createUser(User user);
    void deleteById(Long id);
    User update(User user);
    List<User> getAll();
}
