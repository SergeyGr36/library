package com.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskStarter {
    public static void main(String[] args) {
        SpringApplication.run(TaskStarter.class, args);
    }
}
