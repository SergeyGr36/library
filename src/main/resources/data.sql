drop table if exists user_t CASCADE;
drop table if exists book_t CASCADE;

drop sequence if exists hibernate_sequence;
create sequence hibernate_sequence start with 10 increment by 1;

create table user_t
(
    id         bigint not null auto_increment,
    first_name varchar(255),
    last_name  varchar(255),
    primary key (id)
);

create table book_t
(
    id      bigint not null auto_increment,
    price   numeric(19, 2),
    title   varchar(255),
    user_id bigint,
    primary key (id),
    foreign key (user_id) references user_t (id)
);

insert into user_t (id, first_name, last_name)
VALUES (1, 'Ozzy', 'Osbourne'),
       (2, 'Marilyn', 'Manson'),
       (3, 'Kurt', 'Cobain'),
       (4, 'Chester', 'Bennington');

insert into book_t (id, price, title, user_id)
values (1, 162, 'Holly bible', 2),
       (2, 162, 'gun instruction', 3),
       (3, 162, 'how to prepare batman', 1),
       (4, 162, 'where buy rope and soap', 4),
       (5, 162, 'how to stole David Bowie song', 3);
